#include "Character.h"
#include<iostream>

using namespace std;

void Character::DispHp()
{
	cout << "プレイヤーHP=" << hp << "\n";
}

int Character::Attack(int i)
{
	cout << "プレイヤーの攻撃!";
	return atk - i / 2;
}

void Character::Damage(int i)
{
	cout << "プレイヤーは" << i << "のダメージ\n";
	hp -= i;
}

int Character::GetDef()
{
	return def;
}

bool Character::IsDead()
{
	if (hp <= 0)
		return true;

	return false;
}
