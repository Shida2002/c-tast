#pragma once
class Character
{
protected:
	int hp;
	int atk;
	int def;

public:
	void DispHp();
	int Attack(int i);
	void Damage(int i);
	int GetDef();
	bool IsDead();
};

