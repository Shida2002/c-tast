#include "Player.h"
#include<iostream>

using namespace std;

Player::Player()
{
	hp = 300;
	atk = 50;
	def = 35;
}

void Player::DispHp()
{
	cout << "プレイヤーHP=" << hp << "\n";
}

int Player::Attack(int i)
{
	cout << "プレイヤーの攻撃!";
	return atk-i/2;
}

void Player::Damage(int i)
{
	cout << "プレイヤーは" << i << "のダメージ\n";
	hp -= i;
}

int Player::GetDef()
{
	return def;
}

bool Player::IsDead()
{
	if(hp<=0)
	return true;

	return false;
}
