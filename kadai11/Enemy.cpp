#include "Enemy.h"
#include<iostream>

using namespace std;

Enemy::Enemy()
{
	hp = 200;
	atk = 10;
	def = 40;
}

void Enemy::DispHp()
{
	cout << "プレイヤーHP=" << hp << "\n";
}

int Enemy::Attack(int i)
{
	cout << "プレイヤーの攻撃!";
	return atk - i / 2;
}

void Enemy::Damage(int i)
{
	cout << "プレイヤーは" << i << "のダメージ\n";
	hp -= i;
}

int Enemy::GetDef()
{
	return def;
}

bool Enemy::IsDead()
{
	if (hp <= 0)
		return true;

	return false;
}
