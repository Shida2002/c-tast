/*
TriangleClass.cpp
TriangleClass クラスの各メンバ関数を定義
*/
//ヘッダをインクルード
#include <iostream>
#include "TriangleClass.h"

//底辺と高さを代入
void TriangleClass::Input()
{
	teihen = 20.0f;
	takasa = 15.0f;
}

//計算式
void TriangleClass::Calc()
{
	menseki = teihen * takasa / 2.0f;
}

//結果を出力
void TriangleClass::Disp()
{
	std::cout<< "三角形の面積＝" << menseki << "\n";
}
